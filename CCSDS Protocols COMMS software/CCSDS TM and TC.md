### Channel Architecture
* A **Master Channel** (MC) can include _multiple_ Virtual Channels (VCs). 
* A **Virtual Channel** (VC) can be splitted into many **MAP channels** for TC communication.
![[ChannelArchitecture.excalidraw|1000x400]]
Physical Channel Arguments: 
* `Maximum Frame Length`
* `Error Control Field Present` 
* `Data Rate` 
* `Maximum Number of Repetitions`

TM tranfer frame general structure:
![[Pasted Image 20220902015921_216.png]]

TM VC Arguments: 
* `Maximum Frame Length`
* `Frame Error Control Field Present` 

Primary Header:
![[Pasted Image 20220902020041_307.png]]
* `Master Channel`, `Virtual Channel Id (VCID)` 
* `Operational Control Field TM Present`: CLCW
[^bignote]: CLCW: Communication Link Control Word, 

Data Field Status:
![[Pasted image 20220902132749.png]]
* `Segmentation Header Present`: Indicates whether Segmentation is applied or not
* `Synchronization Flag`:  It shall be ‘0’ if octet-synchronized and forward-ordered Packets or Idle Data are inserted in the Data Field
* `Secondary Header TM Present`: The secondary header can exist or not 
* `Secondary Header TM Length`: It can contain pretty much anything but has to be a standard length for each VC

**Service Channel**: Provides a way to interconnect all different CCSDS Space Data Protocol Services and provides a  bidirectional interface between the receiving and transmitting parties



