/label ~Campaign::Procedures

**Note:** Title the issue should be in the following format:

`[{Visit Date}] {Visit Title}`

(e.g. [7-7-23] COMMS Shock Test, [4-6-24] S-Band end-to-end tests)

## Description

(Brief description of the test and its scope)

### Equipment Checklist

- [ ] Apples
- [ ] Hammer

### Procedures

#### Pre-Test
- [ ] Οpen a markdown file based on the issue so that you can add observations/notes 
- [ ] Set up discord so you can communicate anytime
- [ ] Make sure someone is outside so you have communication with the outside world
- [ ] Wear antistatic braceletes
- [ ] Connect the antistatic bracelets to an electrical outlet 
- [ ] Tape the cables on the desk
- [ ] Unwrap the PCB carefully
- [ ] Screw the pcb to the TVAC adaptor 
    - [ ] place the M3 top on the torque screw driver
    - [ ] adjust 0.5 Nm on the torque screw driver
    - [ ] place the board with the PC104 of the board matching the PC104 mark of the adaptor
    - [ ] adjust 0.5 Nm on the torque screw driver


#### Post-Test

- [ ] Unscrew the pcb from the adaptor, diagonally 
- [ ] Place the pcb in the case as it was and put it in the locker
- [ ] Take all the equipment you brought outside the cleanroom and take them back at lab or leave them there after communicating with other people that might need them (eg OBC) and document them in the cleanroom inventory.
- [ ] Upload the pictures in [this](https://drive.google.com/drive/u/0/folders/1r3l79C9SCFmCSm6knNjGdlqHXCzpLwKL) folder
- [ ] Clean up your notes and upload everything to the issue 
- [ ] Add any todos for the next visit based on the outcomes so that they are included in the procedures

- Mating Cycles

|          | During This Visit | Total Count |
|----------|-------------------|-------------|
| Mezzanine|                   |     /50     |
| FPGA DF11|                   |     /50     |
| MCU DF11 |                   |       /50   |
| M80      |                   |      /500   |



## Operators

- **Operator #1:** Yogger Dev
- **Operator #2:** Yogi Devious
- **PA:** Tsioloforos Christakis
- etc.
